#!/usr/bin/env python
import sys, os
import rospy
import rospkg
from geometry_msgs.msg import TransformStamped, Pose

class viconToTxt():

    def __init__(self):
        self.poseSub = rospy.Subscriber("vrpn_client/raw_transform", 
            TransformStamped, self.viconPoseCallback)
        self.viconPose = Pose()

        self.numberOfPosesAcquired = 0

    def run(self):
        rospy.spin()


    def viconPoseCallback(self, msg):
        self.viconPose.position.x = msg.transform.translation.x
        self.viconPose.position.y = msg.transform.translation.y
        self.viconPose.position.z = msg.transform.translation.z
        self.viconPose.orientation.x = msg.transform.rotation.x
        self.viconPose.orientation.y = msg.transform.rotation.y
        self.viconPose.orientation.z = msg.transform.rotation.z
        self.viconPose.orientation.w = msg.transform.rotation.w

        self.numberOfPosesAcquired = self.numberOfPosesAcquired + 1

        if (self.numberOfPosesAcquired >= 1):
            # Save to file
            rospack = rospkg.RosPack()
            packagePath = rospack.get_path('mav_path_trajectory')
            f = open(packagePath + '/resource/viconPose.txt', 'w')
            timestamp = rospy.Time.now()
            f.write(str(timestamp.secs) + ',' + str(timestamp.nsecs) + ',' + 
                str(self.viconPose.position.x) + ',' + 
                str(self.viconPose.position.y) + ',' + str(self.viconPose.position.z) + ',' + 
                str(self.viconPose.orientation.x) + ',' + str(self.viconPose.orientation.y) + ',' + 
                str(self.viconPose.orientation.z) + ',' + str(self.viconPose.orientation.w))
            f.close()
            rospy.signal_shutdown("Pose acquired")

if __name__=="__main__":
    rospy.init_node("viconToTxt")
    getPose = viconToTxt()
    getPose.run()
