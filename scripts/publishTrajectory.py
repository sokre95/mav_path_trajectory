#!/usr/bin/env python
import sys, os
import rospy
import copy
import math
import time

from geometry_msgs.msg import Pose, Point, Quaternion
from mav_path_trajectory.msg import WaypointArray
from mav_path_trajectory.srv import WaypointArraySrv

class SquareTrajectory():

    def __init__(self):
        self.waypointArray = WaypointArray()
        # Services for requesting trajectory interpolation
        self.waypointArrayPub = rospy.Publisher("waypoints", WaypointArray, 
            queue_size=1)
        time.sleep(0.5)

        self.tempPose = Pose()
        self.tempPose.orientation = copy.deepcopy(Quaternion(0,0,0,1))

        n = 9
        for i in range(0,n):
            self.waypointArray.waypoints.append(copy.deepcopy(self.tempPose))

        
        self.waypointArray.waypoints[0].position = copy.deepcopy(Point(-0.5,-1.5,1))
        self.waypointArray.waypoints[1].position = copy.deepcopy(Point(0.5,-1.5,1))
        self.waypointArray.waypoints[2].position = copy.deepcopy(Point(1.5,-1.5,1))
        self.waypointArray.waypoints[3].position = copy.deepcopy(Point(1.5,0,1))
        self.waypointArray.waypoints[4].position = copy.deepcopy(Point(1.5,1.5,1))
        self.waypointArray.waypoints[5].position = copy.deepcopy(Point(0.5,1.5,1))
        self.waypointArray.waypoints[6].position = copy.deepcopy(Point(-0.5,1.5,1))
        self.waypointArray.waypoints[7].position = copy.deepcopy(Point(-0.5,0,1))
        self.waypointArray.waypoints[8].position = copy.deepcopy(Point(-0.5,-1.5,1))
        
        """
        self.waypointArray.waypoints[0].position = copy.deepcopy(Point(0,0,1))
        self.waypointArray.waypoints[1].position = copy.deepcopy(Point(0.25,0,1))
        self.waypointArray.waypoints[2].position = copy.deepcopy(Point(0.5,0,1))
        self.waypointArray.waypoints[3].position = copy.deepcopy(Point(0.5,1.5,1))
        self.waypointArray.waypoints[4].position = copy.deepcopy(Point(0.5,3,1))
        self.waypointArray.waypoints[5].position = copy.deepcopy(Point(0.25,3,1))
        self.waypointArray.waypoints[6].position = copy.deepcopy(Point(0,3,1))
        self.waypointArray.waypoints[7].position = copy.deepcopy(Point(0,1.5,1))
        self.waypointArray.waypoints[8].position = copy.deepcopy(Point(0,0,1))
        """
        """
        self.waypointArray.waypoints[0].position = copy.deepcopy(Point(0.5,0,1))
        self.waypointArray.waypoints[1].position = copy.deepcopy(Point(0.5,1.5,1))
        self.waypointArray.waypoints[2].position = copy.deepcopy(Point(1,3,1))
        self.waypointArray.waypoints[3].position = copy.deepcopy(Point(1,1.5,1))
        """
        self.waypointArrayPub.publish(self.waypointArray)
        #print self.waypointArray
        print "Message published"


if __name__=="__main__":
    rospy.init_node("NeoSquareTrajectory")
    SquareTrajectory()


