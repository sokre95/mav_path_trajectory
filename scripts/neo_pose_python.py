#!/usr/bin/env python
import sys, os
import rospy

from geometry_msgs.msg import PoseStamped

class pubPose():

	def __init__(self):
		self.pub = rospy.Publisher("/euroc3/command/pose", PoseStamped, queue_size=1)
		self.poseStamped = PoseStamped()
		self.sub = rospy.Subscriber("/euroc3/command/pose2", PoseStamped, self.callback)
		self.seq = 0
		rospy.spin()

	def callback(self, data):
		self.poseStamped = data
		self.poseStamped.header.stamp = rospy.Time.now() + rospy.Duration(1)
		self.poseStamped.header.seq = self.seq
		self.seq = self.seq + 1
		self.pub.publish(self.poseStamped)
		print "Published messages: ", self.seq

if __name__=="__main__":
	rospy.init_node("pose_publisher")
	klasa = pubPose()