#!/usr/bin/env python
import sys, os
import rospy
from mav_path_trajectory.msg import TrajectorySampled
from geometry_msgs.msg import PoseStamped
from nav_msgs.msg import Path
import tf
from geometry_msgs.msg import TransformStamped
from geometry_msgs.msg import PointStamped
from visualization_msgs.msg import Marker, MarkerArray


class displayTrajectoryInRviz():

	def __init__(self):
		# Subscribes to trajectory and stores it in message below
		self.trajectorySampledSub = rospy.Subscriber(
			"trajectory", TrajectorySampled, 
			self.trajectorySampledCallback)
		self.trajectorySampled = TrajectorySampled()

		# Loop rate determines display rate of path in rviz
		self.loopRate = rospy.get_param("loop_rate", 30)
		

		# Path is sent to Rviz for display
		self.pathPub = rospy.Publisher("path", Path, queue_size=1)
		self.path = Path()
		for i in range(2):
			tempPoseStamped = PoseStamped()
			self.path.poses.append(tempPoseStamped)
		# In addition, current position of robot can be displayed, in that case
		# subscriber to position feedback will be needed
		#self.pointPub = rospy.Publisher("position", PointStamped, queue_size=1)


	def run(self):
		
		"""
		self.tempPoseStamped1 = PoseStamped()
		self.tempPoseStamped1.pose.position.x = 0
		self.tempPoseStamped1.pose.position.y = 0
		self.tempPoseStamped1.pose.position.z = 0
		self.tempPoseStamped1.pose.orientation.w = 1.0
		self.tempPoseStamped1.header.stamp = rospy.Time.now()
		self.tempPoseStamped1.header.frame_id = "world"
		self.path.poses.append(self.tempPoseStamped1)

		print self.path

		self.tempPoseStamped.pose.position.x = 1
		self.tempPoseStamped.pose.position.y = 1
		self.tempPoseStamped.pose.position.z = 1
		self.tempPoseStamped.pose.orientation.w = 1.0
		self.tempPoseStamped.header.stamp = rospy.Time.now()
		self.tempPoseStamped.header.frame_id = "world"
		self.path.poses.append(self.tempPoseStamped)
		print self.path
		"""


		r = rospy.Rate(self.loopRate)
		while not rospy.is_shutdown():
			#pt = PointStamped()
			#pt.header.stamp = rospy.Time.now()
			#pt.header.frame_id = "world"
			#self.pointPub.publish(pt)

			self.path.header.stamp = rospy.Time.now()
			self.path.header.frame_id = "world"
			self.pathPub.publish(self.path)

			r.sleep()


	def trajectorySampledCallback(self, data):
		self.trajectorySampled = data
		print len(self.trajectorySampled.position)

		self.path.poses = []
		for i in range(len(self.trajectorySampled.position)):
			tempPoseStamped = PoseStamped()
			tempPoseStamped.pose.position.x = self.trajectorySampled.position[i].x
			tempPoseStamped.pose.position.y = self.trajectorySampled.position[i].y
			tempPoseStamped.pose.position.z = self.trajectorySampled.position[i].z
			tempPoseStamped.pose.orientation.w = 1.0
			tempPoseStamped.header.stamp = rospy.Time.now()
			tempPoseStamped.header.frame_id = "world"
			self.path.poses.append(tempPoseStamped)
		


if __name__ == "__main__":
	rospy.init_node('displayTrajectoryInRviz')
	a = displayTrajectoryInRviz()
	a.run()