#!/usr/bin/env python
import sys, os
import rospy
from geometry_msgs.msg import Pose, TransformStamped
import time
import math
from trajectory_msgs.msg import MultiDOFJointTrajectory
from std_msgs.msg import Empty
from nav_msgs.msg import Odometry

from std_srvs.srv import SetBool, SetBoolResponse
from mav_path_trajectory.srv import ReturnRMS, ReturnRMSResponse

import copy

class computeRMS():

	def __init__(self):
		rospy.Subscriber("vrpn_client/raw_transform", TransformStamped, self.odometryCallback)
		self.odometry = Odometry()

		rospy.Subscriber("command/current_reference", MultiDOFJointTrajectory, 
			self.referenceCallback)
		self.reference = Pose()

		# Service for start recording
		self.startComputingRmsService = rospy.Service("startComputingRMS", 
			SetBool, self.startComputingRmsCallback)
		self.computingRmsFlag = False

		# Service for stop recording
		self.stopComputingRmsService = rospy.Service("stopComputingRMS", 
			ReturnRMS, self.stopComputingRmsCallback)

		# rms messages counter
		self.rmsCounter = 0.0

		# initialize rms values
		self.xSqrSum = 0.0
		self.ySqrSum = 0.0
		self.zSqrSum = 0.0
		self.allSqrSum = 0.0

		self.xRMS = 0.0
		self.yRMS = 0.0
		self.zRMS = 0.0
		self.allRMS = 0.0

		self.xMAX = 0.0
		self.yMAX = 0.0
		self.zMAX = 0.0
		self.allMAX = 0.0


		self.rate = rospy.get_param('~rate', 200)


	def run(self):
		r = rospy.Rate(self.rate)

		while not rospy.is_shutdown():

			if self.computingRmsFlag == True:
				self.rmsCounter = self.rmsCounter + 1.0
				xDiffSqr = pow(self.odometry.pose.pose.position.x - 
					self.reference.position.x, 2.0)
				yDiffSqr = pow(self.odometry.pose.pose.position.y - 
					self.reference.position.y, 2.0)
				zDiffSqr = pow(self.odometry.pose.pose.position.z - 
					self.reference.position.z, 2.0)
				allDiffSqr = xDiffSqr + yDiffSqr + zDiffSqr

				if (pow(xDiffSqr, 0.5) > self.xMAX):
					self.xMAX = pow(xDiffSqr, 0.5)
				if (pow(yDiffSqr, 0.5) > self.yMAX):
					self.yMAX = pow(yDiffSqr, 0.5)
				if (pow(zDiffSqr, 0.5) > self.zMAX):
					self.zMAX = pow(zDiffSqr, 0.5)
				if (pow(allDiffSqr, 0.5) > self.allMAX):
					self.allMAX = pow(allDiffSqr, 0.5)

				self.xSqrSum = self.xSqrSum + xDiffSqr
				self.ySqrSum = self.ySqrSum + yDiffSqr
				self.zSqrSum = self.zSqrSum + zDiffSqr
				self.allSqrSum = self.allSqrSum + allDiffSqr

				self.xRMS = pow(self.xSqrSum/self.rmsCounter, 0.5)
				self.yRMS = pow(self.ySqrSum/self.rmsCounter, 0.5)
				self.zRMS = pow(self.zSqrSum/self.rmsCounter, 0.5)
				self.allRMS = pow(self.allSqrSum/self.rmsCounter, 0.5)

			r.sleep()


	def odometryCallback(self, msg):
		#self.odometry = msg
		self.odometry.pose.pose.position.x = msg.transform.translation.x
		self.odometry.pose.pose.position.y = msg.transform.translation.y
		self.odometry.pose.pose.position.z = msg.transform.translation.z

	def referenceCallback(self, msg):
		self.reference.position.x = msg.points[0].transforms[0].translation.x
		self.reference.position.y = msg.points[0].transforms[0].translation.y
		self.reference.position.z = msg.points[0].transforms[0].translation.z

	def startComputingRmsCallback(self, req):

		self.computingRmsFlag = True
		print "Started RMS computation"
		return SetBoolResponse(True, "Computing Started")

	def stopComputingRmsCallback(self, req):
		tmpRMSx = copy.deepcopy(self.xRMS)
		tmpRMSy = copy.deepcopy(self.yRMS)
		tmpRMSz = copy.deepcopy(self.zRMS)
		tmpRMSall = copy.deepcopy(self.allRMS)
		tmpMAXx = copy.deepcopy(self.xMAX)
		tmpMAXy = copy.deepcopy(self.yMAX)
		tmpMAXz = copy.deepcopy(self.zMAX)
		tmpMAXall = copy.deepcopy(self.allMAX)

		print "xRMS: ", self.xRMS
		print "yRMS: ", self.yRMS
		print "zRMS: ", self.zRMS
		print "allRMS: ", self.allRMS
		print "xMAX: ", self.xMAX
		print "yMAX: ", self.yMAX
		print "zMAX: ", self.zMAX
		print "allMAX: ", self.allMAX

		self.rmsCounter = 0.0
		self.xSqrSum = 0.0
		self.ySqrSum = 0.0
		self.zSqrSum = 0.0
		self.sqrSumAll = 0.0

		self.xRMS = 0.0
		self.yRMS = 0.0
		self.zRMS = 0.0
		self.allRMS = 0.0
		self.xMAX = 0.0
		self.yMAX = 0.0
		self.zMAX = 0.0
		self.allMAX = 0.0
		self.computingRmsFlag = False

		print "Stopped RMS computation"
		print "-----------------------"
		return ReturnRMSResponse(tmpRMSx, tmpRMSy, tmpRMSz, tmpRMSall, 
			tmpMAXx, tmpMAXy, tmpMAXz, tmpMAXall, "Computing Stopped")



if __name__=="__main__":
	rospy.init_node("computeRMS")
	rmsComputation = computeRMS()
	rmsComputation.run()