#!/usr/bin/env python
import sys, os
import time
import math
import copy

import rospy
from mav_path_trajectory.msg import WaypointArray
from visualization_msgs.msg import Marker
from nav_msgs.msg import Path
from geometry_msgs.msg import Pose, Point
from std_msgs.msg import ColorRGBA
from mav_path_trajectory.srv import SendHelixParams
from mav_path_trajectory.srv import WaypointArraySrv

class HelicalTrajectory():

    def __init__(self):
        # Ros publisher for markers
        self.markerPub = rospy.Publisher("helical_trajectory/marker", 
            Marker, queue_size=1)
        self.marker = Marker()

        # Create service to receive helical trajectory params
        self.helixParamsService = rospy.Service('helix_params', SendHelixParams, 
            self.helixParamsServiceCallback)

        # Waypoints service
        rospy.wait_for_service("positionWaypointArray", timeout=30)
        self.waypointArrayService = rospy.ServiceProxy("positionWaypointArray", 
            WaypointArraySrv)

    def run(self):
        rospy.spin()

    def helixParamsServiceCallback(self, req):
        r = req.r
        angleStep = req.angleStep
        x0 = req.x0
        y0 = req.y0
        z0 = req.z0
        zf = req.zf
        deltaZ = req.deltaZ

        # Create waypoint array and pose messages
        wp = WaypointArray()
        tempPose = Pose()
        n = int((zf-z0)/deltaZ)

        for i in range(n+1):
            x = r*math.cos(float(i)*angleStep) + x0
            y = r*math.sin(float(i)*angleStep) + y0
            z = z0 + float(i)*deltaZ
            tempPose.position.x = x
            tempPose.position.y = y
            tempPose.position.z = z
            tempPose.orientation.w = 1.0
            wp.waypoints.append(copy.deepcopy(tempPose))


        # Create helix for visualization in rviz
        tempMarker = Marker()
        tempMarker.type = Marker.SPHERE_LIST
        tempMarker.action = Marker.ADD
        tempMarker.header.stamp = rospy.Time.now()
        tempMarker.header.frame_id = "world"
        tempMarker.ns = "helix"
        tempMarker.id = 0
        tempMarker.scale.x = 0.07
        tempMarker.scale.y = 0.07
        tempMarker.scale.z = 0.07
        tempMarker.color.r = 1.0
        tempMarker.color.a = 1.0
        tempMarker.lifetime = rospy.Duration()
        tempMarker.pose.orientation.w = 1.0

        tempColor = ColorRGBA()
        tempColor.r = 1.0
        tempColor.a = 1.0

        tempPoint = Point()

        for i in range(n+1):
            tempPoint.x = wp.waypoints[i].position.x
            tempPoint.y = wp.waypoints[i].position.y
            tempPoint.z = wp.waypoints[i].position.z
            tempMarker.points.append(copy.deepcopy(tempPoint))
            tempMarker.colors.append(copy.deepcopy(tempColor))


        self.markerPub.publish(tempMarker)
        self.waypointArrayService(wp)
        print wp.waypoints[0]
        #self.addTwoMoreWaypoints(tempMarker, wp, req.epsilon)

        return None


    def addTwoMoreWaypoints(self, marker, wp, epsilon):
        tempMarker = Marker()
        tempWp = WaypointArray()

        m = len(wp.waypoints)
        print m

        vector1 = Point()
        vector1.x = wp.waypoints[1].position.x - wp.waypoints[0].position.x
        vector1.y = wp.waypoints[1].position.y - wp.waypoints[0].position.y
        vector1.z = wp.waypoints[1].position.z - wp.waypoints[0].position.z
        norm1 = math.sqrt(pow(vector1.x, 2) + pow(vector1.y, 2) + pow(vector1.z, 2))
        vector1.x = vector1.x/norm1
        vector1.y = vector1.y/norm1
        vector1.z = vector1.z/norm1
        point1 = Point()
        point1.x = wp.waypoints[0].position.x + vector1.x*norm1*epsilon
        point1.y = wp.waypoints[0].position.y + vector1.y*norm1*epsilon
        point1.z = wp.waypoints[0].position.z + vector1.z*norm1*epsilon

        #print wp.waypoints[0].position, wp.waypoints[1].position, vector1, point1

        vectorM = Point()
        vectorM.x = wp.waypoints[m-1].position.x - wp.waypoints[m-2].position.x
        vectorM.y = wp.waypoints[m-1].position.y - wp.waypoints[m-2].position.y
        vectorM.z = wp.waypoints[m-1].position.z - wp.waypoints[m-2].position.z
        normM = math.sqrt(pow(vectorM.x, 2) + pow(vectorM.y, 2) + pow(vectorM.z, 2))
        vectorM.x = vectorM.x/normM
        vectorM.y = vectorM.y/normM
        vectorM.z = vectorM.z/normM
        pointM = Point()
        pointM.x = wp.waypoints[m-1].position.x - vectorM.x*normM*epsilon
        pointM.y = wp.waypoints[m-1].position.y - vectorM.y*normM*epsilon
        pointM.z = wp.waypoints[m-1].position.z - vectorM.z*normM*epsilon

        # assign values to tempMarker
        tempMarker.type = Marker.SPHERE_LIST
        tempMarker.action = Marker.ADD
        tempMarker.header.stamp = rospy.Time.now()
        tempMarker.header.frame_id = "world"
        tempMarker.ns = "helix"
        tempMarker.id = 0
        tempMarker.scale.x = 0.07
        tempMarker.scale.y = 0.07
        tempMarker.scale.z = 0.07
        tempMarker.color.r = 1.0
        tempMarker.color.a = 1.0
        tempMarker.lifetime = rospy.Duration()
        tempMarker.pose.orientation.w = 1.0

        tempColor = ColorRGBA()
        tempColor.r = 1.0
        tempColor.a = 1.0

        tempPoint = Point()

        tempPose = Pose()
        for i in range(0, m):
            if (i==1):
                tempPose.orientation.w = 1
                tempPose.position.x = point1.x
                tempPose.position.y = point1.y
                tempPose.position.z = point1.z
                tempWp.waypoints.append(copy.deepcopy(tempPose))

                tempPoint.x = point1.x
                tempPoint.y = point1.y
                tempPoint.z = point1.z
                tempMarker.points.append(copy.deepcopy(tempPoint))
                tempMarker.colors.append(copy.deepcopy(tempColor))
            elif (i==m-1):
                tempPose.orientation.w = 1
                tempPose.position.x = pointM.x
                tempPose.position.y = pointM.y
                tempPose.position.z = pointM.z
                tempWp.waypoints.append(copy.deepcopy(tempPose))

                tempPoint.x = pointM.x
                tempPoint.y = pointM.y
                tempPoint.z = pointM.z
                tempMarker.points.append(copy.deepcopy(tempPoint))
                tempMarker.colors.append(copy.deepcopy(tempColor))

            tempPose.orientation.w = 1
            tempPose.position = copy.deepcopy(wp.waypoints[i].position)
            tempWp.waypoints.append(copy.deepcopy(tempPose))
            tempPoint.x = wp.waypoints[i].position.x
            tempPoint.y = wp.waypoints[i].position.y
            tempPoint.z = wp.waypoints[i].position.z
            tempMarker.points.append(copy.deepcopy(tempPoint))
            tempMarker.colors.append(copy.deepcopy(tempColor))


        self.markerPub.publish(tempMarker)
        self.waypointArrayService(tempWp)
        print tempWp.waypoints[0]


if __name__=="__main__":
    rospy.init_node('HelicalTrajectory')
    a = HelicalTrajectory()
    a.run()