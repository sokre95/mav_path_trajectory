#!/usr/bin/env python
import sys, os
import rospy
import copy
import math
import time

from geometry_msgs.msg import Pose, Point, Quaternion
from mav_path_trajectory.msg import WaypointArray
from mav_path_trajectory.srv import WaypointArraySrv

class TrajectoryPublisher():

    def __init__(self):
        self.waypointArray = WaypointArray()
        # Services for requesting trajectory interpolation
        self.waypointArrayPub = rospy.Publisher("waypoints", WaypointArray, 
            queue_size=1)
        rospy.wait_for_service("positionWaypointArray", timeout=30)
        self.waypointArrayService = rospy.ServiceProxy("positionWaypointArray",
            WaypointArraySrv)

        self.points = []
        self.points.append(Point(0,0,0.1))
        self.points.append(Point(0,0,1))
        self.points.append(Point(20,0,9))
        self.points.append(Point(20,20,1))
        self.points.append(Point(0,20,9))
        self.points.append(Point(0,0,1))
        time.sleep(0.5)

        self.tempPose = Pose()
        self.tempPose.orientation = copy.deepcopy(Quaternion(0,0,0,1))

        rospy.sleep(2)

        for i in range(0,2):
            self.waypointArray.waypoints.append(copy.deepcopy(self.tempPose))

        for i in range(5):
            self.waypointArray.waypoints[0].position = copy.deepcopy(self.points[i])
            self.waypointArray.waypoints[1].position = copy.deepcopy(self.points[i+1])

            #self.waypointArrayPub.publish(self.waypointArray)
            self.waypointArrayService(self.waypointArray)
            print "Message published"
            rospy.sleep(30)



if __name__=="__main__":
    rospy.init_node("trajectory_publisher")
    TrajectoryPublisher()


