#!/usr/bin/env python
import sys, os, copy
import math, time

from PyQt4 import QtCore, QtGui, uic
import rospy, rospkg
from std_msgs.msg import String, Float64
from geometry_msgs.msg import Pose, PoseStamped
from mav_path_trajectory.msg import WaypointArray
from mav_path_trajectory.srv import WaypointArraySrv
from trajectory_msgs.msg import MultiDOFJointTrajectory

class SendWaypoints():
	# This class sends two waypoints to the mission planner in order to
	# generate trajectory to the desired point

	def __init__(self):
		# First get path and create window object
		app = QtGui.QApplication(sys.argv)
		rospack = rospkg.RosPack()
		path = rospack.get_path('mav_path_trajectory')
		self.window = uic.loadUi(path + "/resource/SendWaypoints.ui")
		self.window.show()

		# Set up button callbacks
		self.window.pushButtonSetPos.clicked.connect(self.pushButtonSetPosCallback)
		self.window.pushButtonSetOri.clicked.connect(self.pushButtonSetOriCallback)
		self.window.pushButtonSendWaypoints.clicked.connect(
			self.pushButtonSendWaypointsCallback)
		self.window.pushButtonSendYaw0.clicked.connect(self.pushButtonSendYaw0Callback)

		# WaypointArray service
		rospy.wait_for_service("positionWaypointArray", timeout=30)
		self.waypointArrayService = rospy.ServiceProxy("positionWaypointArray", 
			WaypointArraySrv)

		# Current reference subscriber
		rospy.Subscriber("/euroc3/command/current_reference", 
			MultiDOFJointTrajectory, self.currentReferenceCallback)
		self.currentReference = MultiDOFJointTrajectory()

		# PoseStamped publisher
		self.commandPosePub = rospy.Publisher("/euroc3/command/pose", 
			PoseStamped, queue_size=1)

		# Set up start and end pose
		self.startPose = copy.deepcopy(Pose())
		self.startPose.orientation.w = 1.0
		self.endPose = copy.deepcopy(Pose())
		self.endPose.orientation.w = 1.0

		# exit gui stuff
		sys.exit(app.exec_())

	def currentReferenceCallback(self, data):
		#print data.points[0].transforms[0].translation
		self.currentReference = data

	def pushButtonSetPosCallback(self):
		# Reads current position from euroc3/command/current_reference
		# and sets it as start position
		self.startPose.position.x = \
			self.currentReference.points[0].transforms[0].translation.x
		self.startPose.position.y = \
			self.currentReference.points[0].transforms[0].translation.y
		self.startPose.position.z = \
			self.currentReference.points[0].transforms[0].translation.z

		self.startPose.orientation.x = \
			self.currentReference.points[0].transforms[0].rotation.x
		self.startPose.orientation.y = \
			self.currentReference.points[0].transforms[0].rotation.y
		self.startPose.orientation.z = \
			self.currentReference.points[0].transforms[0].rotation.z
		self.startPose.orientation.w = \
			self.currentReference.points[0].transforms[0].rotation.w

		self.window.lineEditStartPosX.setText(str(self.startPose.position.x))
		self.window.lineEditStartPosY.setText(str(self.startPose.position.y))
		self.window.lineEditStartPosZ.setText(str(self.startPose.position.z))
		self.window.lineEditStartOriX.setText(str(self.startPose.orientation.x))
		self.window.lineEditStartOriY.setText(str(self.startPose.orientation.y))
		self.window.lineEditStartOriZ.setText(str(self.startPose.orientation.z))
		self.window.lineEditStartOriW.setText(str(self.startPose.orientation.w))


	def pushButtonSetOriCallback(self):
		self.window.lineEditStartOriX.setText("0.0")
		self.window.lineEditStartOriY.setText("0.0")
		self.window.lineEditStartOriZ.setText("0.0")
		self.window.lineEditStartOriW.setText("1.0")
		self.window.lineEditEndOriX.setText("0.0")
		self.window.lineEditEndOriY.setText("0.0")
		self.window.lineEditEndOriZ.setText("0.0")
		self.window.lineEditEndOriW.setText("1.0")

	def pushButtonSendWaypointsCallback(self):
		self.updatePoses()

		tempWaypointArray = WaypointArray()
		tempWaypointArray.waypoints.append(self.startPose)
		tempWaypointArray.waypoints.append(self.endPose)
		self.waypointArrayService(tempWaypointArray)

	def pushButtonSendYaw0Callback(self):
		# Reads current position from euroc3/command/current_reference
		# and sets it as start position
		self.startPose.position.x = \
			self.currentReference.points[0].transforms[0].translation.x
		self.startPose.position.y = \
			self.currentReference.points[0].transforms[0].translation.y
		self.startPose.position.z = \
			self.currentReference.points[0].transforms[0].translation.z

		self.startPose.orientation.x = 0
		self.startPose.orientation.y = 0
		self.startPose.orientation.z = 0
		self.startPose.orientation.w = 1

		self.window.lineEditStartPosX.setText(str(self.startPose.position.x))
		self.window.lineEditStartPosY.setText(str(self.startPose.position.y))
		self.window.lineEditStartPosZ.setText(str(self.startPose.position.z))
		self.window.lineEditStartOriX.setText(str(self.startPose.orientation.x))
		self.window.lineEditStartOriY.setText(str(self.startPose.orientation.y))
		self.window.lineEditStartOriZ.setText(str(self.startPose.orientation.z))
		self.window.lineEditStartOriW.setText(str(self.startPose.orientation.w))

		tempPoseStamped = PoseStamped()
		tempPoseStamped.pose = copy.deepcopy(self.startPose)
		self.commandPosePub.publish(tempPoseStamped)



	def updatePoses(self):
		self.startPose.position.x = float(str(self.window.lineEditStartPosX.text()))
		self.startPose.position.y = float(str(self.window.lineEditStartPosY.text()))
		self.startPose.position.z = float(str(self.window.lineEditStartPosZ.text()))
		self.startPose.orientation.x = float(str(self.window.lineEditStartOriX.text()))
		self.startPose.orientation.y = float(str(self.window.lineEditStartOriY.text()))
		self.startPose.orientation.z = float(str(self.window.lineEditStartOriZ.text()))
		self.startPose.orientation.w = float(str(self.window.lineEditStartOriW.text()))

		self.endPose.position.x = float(str(self.window.lineEditEndPosX.text()))
		self.endPose.position.y = float(str(self.window.lineEditEndPosY.text()))
		self.endPose.position.z = float(str(self.window.lineEditEndPosZ.text()))
		self.endPose.orientation.x = float(str(self.window.lineEditEndOriX.text()))
		self.endPose.orientation.y = float(str(self.window.lineEditEndOriY.text()))
		self.endPose.orientation.z = float(str(self.window.lineEditEndOriZ.text()))
		self.endPose.orientation.w = float(str(self.window.lineEditEndOriW.text()))

if __name__ == "__main__":
	rospy.init_node("SendWaypointsGUI")
	Gui = SendWaypoints()